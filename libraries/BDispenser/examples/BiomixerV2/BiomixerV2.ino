#include "BDispenser.h"
#include "HX711.h"
#include "max6675.h"

// dispenser parameters

#define pd1m1 2
#define pd2m1 3
#define ppwmm1 4
#define Kpm1 1

#define pd1m2 5
#define pd2m2 6
#define ppwmm2 7
#define Kpm2 1

#define pd1m3 8
#define pd2m3 9
#define ppwmm3 10
#define Kpm3 1

#define pd1m4 A0
#define pd2m4 A1
#define ppwmm4 11
#define Kpm4 1

#define pd1m5 A2
#define pd2m5 A3
#define ppwmm5 12
#define Kpm5 1

BDispenser m1(pd1m1,pd2m1,ppwmm1);
BDispenser m2(pd1m2,pd2m2,ppwmm2);
BDispenser m3(pd1m3,pd2m3,ppwmm3);
BDispenser m4(pd1m4,pd2m4,ppwmm4);
BDispenser m5(pd1m5,pd2m5,ppwmm5);

BDispenser dispensers[5]={m1,m2,m3,m4,m5};

// balance parameters

float setg = 0;

#define CC_CLK A8
#define CC_DAT A9
#define calibrationfactor 965500 // 1 kg load cell

unsigned long sampletime = 50;

HX711 Balance(CC_CLK,CC_DAT);

// themocuple parameters

#define thermoDO A4
#define thermoCS A5
#define thermoCLK A6

MAX6675 tsensor(thermoCLK, thermoCS, thermoDO);

float temp = 0;
unsigned long stime = 2000;
unsigned long st = 0;

// buzzer parameters

#define buzzpin A7

// rgb parameters

#define Rpin 44
#define Gpin 45
#define Bpin 46
int rstate = 0;
int gstate = 0;
int bstate = 0;

// machine parameters

int nstages = 5;
int stage = 0;
float settemp = 0;
boolean dispensing = false;
boolean mixing = false;
boolean waitsignal = false;
unsigned long readybeep = 1000;
boolean yestone = true;

void setupbalance(){ 
  
  Balance.set_scale();
  Balance.tare();
  Balance.read_average();
  Balance.set_scale(calibrationfactor);
  Balance.tare();  
  
}

void setupbuzzer(){

  pinMode(buzzpin,OUTPUT);
  
}

void setuprgb(){
  
  pinMode(Rpin,OUTPUT);
  pinMode(Gpin,OUTPUT);  
  pinMode(Bpin,OUTPUT);
  rstate=0; 
  gstate=1;
  bstate=0;
  
}

void lights(){
  
  digitalWrite(Rpin,rstate);
  digitalWrite(Gpin,gstate);
  digitalWrite(Bpin,bstate);
  
}

void setupdispensers(){

dispensers[0].setkp(Kpm1);
dispensers[0].assign("pump","water",1.0);
dispensers[1].setkp(Kpm2);
dispensers[1].assign("ppump","glycerine",0.7);
dispensers[2].setkp(Kpm3);
dispensers[2].assign("powderd","agar-agar",1.0);
dispensers[3].setkp(Kpm4);
dispensers[3].assign("powderd","CA propanoate",1.0);   
dispensers[4].setkp(Kpm5);
dispensers[4].assign("powderd","apple",1.0);
  
}

void setup() {

  setupbalance();
  setupbuzzer();
  setuprgb();
  setupdispensers();
  Serial.begin(9600);
  Serial.println("BIOMIXER V2 SETTING UP");
  delay(500);
  Serial.println("READY FOR MAKING");
  tone(buzzpin,440,500);
  delay(1000);
  noTone(buzzpin);
  //message to user
  
  Serial.print("How much ");
  Serial.print(dispensers[stage].bmaterial()); 
  if(dispensers[stage].btype()=="powder"){
    Serial.println("[gr]?: ");
  }
  else{
    Serial.println("[ml]?: ");
  }

}

void loop() {

  lights();

  if(stage<nstages){ //dispensing stage
    
  if(Serial.available()>0 && dispensing==false){
    setg=Serial.parseFloat();
          Serial.flush();
    if(setg>0){
    dispensing=true;
    Balance.tare();
    }
  }
  
  if(dispensing==true){


    if (millis()-st>=sampletime){
      float grams = Balance.get_units()*1000;
      Serial.print(setg);
      Serial.print("     ");
      Serial.println(grams);
      dispensers[stage].dispense(setg,grams);
      st=millis();
      rstate=1;
      gstate=0;
      bstate=0;
    } 
   
    if(dispensers[stage].isReady()==true){
      dispensing=false;
      dispensers[stage].turnoff();
      if(dispensers[stage].btype()!="pump"){
        Serial.println("RETRACTING");
        dispensers[stage].retraction();
        delay(3000);
        dispensers[stage].turnoff();
      }
      setg=50000;
      stage=stage+1;
      tone(buzzpin,440,500);
    delay(1000);
    noTone(buzzpin);
    rstate=0;
    gstate=0;
    bstate=1;
      
    if(stage!=nstages){   // ask user again
    Serial.print("How much ");
    Serial.print(dispensers[stage].bmaterial()); 
    if(dispensers[stage].btype()=="powder"){
      Serial.println("[gr]?: ");
    }
    else{
      Serial.println("[ml]?: ");
    }
    }
    else{  // all ingredients dispensed
      Serial.println("Target temperature for mixing[°C]?: ");
    }
    }
  }
  }
    
  else{  
    
    //mixing stage
    if(Serial.available()>0 && mixing==false){
      settemp=Serial.parseFloat();
      if(settemp>0){
      mixing=true;      
      rstate=1;
      gstate=0;
      bstate=0;
      }
    }
    
    if(mixing==true){

      if(millis()-st>=stime && waitsignal==false){
              temp=tsensor.readCelsius();
              Serial.println(temp);
              st=millis();
      }
      
      if(temp>=settemp && waitsignal==false){
        waitsignal=true;
        settemp=0;
        Serial.println("REMOVE VASE AND SEND ANY CHAR");

      }

      if(millis()-st>=readybeep && waitsignal==true){
        yestone!=yestone;
        st=millis();
      }

      if(waitsignal==true){
        if(yestone==true){
          tone(buzzpin,440,1000);
        }
        else{
          noTone(buzzpin);
        }
      }

      if(waitsignal==true && Serial.available()>0){
        mixing=false;
        waitsignal=false;
      }

      if (mixing==false){
        
        stage=0;
        tone(buzzpin,440,500);
        delay(1000);
        noTone(buzzpin);
        rstate=0;
        gstate=1;
        bstate=0;
        Serial.println("MIXING COMPLETE");
        Serial.println("-----------------");
        Serial.println("Ready for making");
        Serial.print("How much ");
        Serial.print(dispensers[stage].bmaterial()); 
        if(dispensers[stage].btype()=="powder"){
          Serial.println("[gr]?: ");
        }
        else{
          Serial.println("[ml]?: ");
        }
      }
    }
    
  }
        Serial.flush();
}
