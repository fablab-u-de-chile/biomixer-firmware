#!/bin/sh

cd ~/Arduino/libraries

ln -s ~/repos/biomixer-firmware/libraries/BDispenser ~/Arduino/libraries/BDispenser
ln -s ~/repos/biomixer-firmware/libraries/HX711 ~/Arduino/libraries/HX711
ln -s ~/repos/biomixer-firmware/libraries/MAX6675 ~/Arduino/libraries/MAX6675
