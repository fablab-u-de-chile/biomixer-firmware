# biomixer-firmware

Firmware para el Arduino Mega que permite controlar la Biomixer mediante la interfaz web o por el monitor serial del Arduino IDE.



## Código fuente

El código fuente para manejar la biomixer desde el **servidor web** lo puedes encontrar acá -> [biomixer_web.ino](src/biomixer_web/biomixer_web.ino).

También temenos un código que permite controlar la Biomixer directamente desde el **monitor serial** del Arduino, lo puedes encontrar acá -> [biomixer_serial.ino](src/biomixer_serial/biomixer_serial.ino).

## Librerias

Para compilar los códigos son necesarias las librerías

* [HX711](https://github.com/juano2310/HX711ADC) Para medir la celda de carga
* [MAX6675](https://github.com/adafruit/MAX6675-library) Para medir la termocupla tipo k
* [BDispenser]() Para el control de los dispensadores

Pueden encontrar una copia de las librerías [acá](libraries/).

## Arduino CLI

El firmware puede ser cargado en el arduino directamente desde la Raspberry de la Biomixer utilizando el Arduino Command Line Interface (CLI)

Para compilar el [código de arduino](src/biomixer_web/biomixer_web.ino) se debe ejecutar el siguiente comando:

    arduino-cli compile --fqbn arduino:avr:mega

Para cargar el código compilado al arduino se debe ejecutar el siguiente comando:

    arduino-cli upload -b arduino:avr:mega -p /dev/ttyACM0 -v
