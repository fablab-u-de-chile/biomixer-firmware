#include "BDispenser.h"
#include "HX711.h"
#include "max6675.h"

#define CMD_STOP          2
#define CMD_START         1
#define CMD_SET_VALUES    5
#define CMD_RGB_DEBUG     6

#define ST_IDLE           0
#define ST_DISPENSING     1
#define ST_MIXING         2
#define ST_WAIT_TEMP      3
#define ST_END_PROCCESS   4

#define N_STAGES 5 // etapas de alimentacion de la biomixer, una por motor

#define numBytes 11
#define dataValues 5
union {
  char bytes[numBytes];
  struct {
    uint8_t cmd;
    // int in arduino are 2 bytes long
    uint16_t data[5];
  } unpacked;
} packet;

uint16_t values[5] = {0, 0, 0, 0, 0};

void readPacket() {
  if (Serial.available() < numBytes);
  // do nothing and wait if serial buffer doesn't have enough bytes to read
  Serial.readBytes(packet.bytes, numBytes);
  // read numBytes bytes from serial buffer and store them at a
  // union called “packet”
}

// dispenser parameters
// motor 1
#define pd1m1 A0
#define pd2m1 A1
#define ppwmm1 11
#define Kpm1 25

// motor 2
#define pd1m2 A2
#define pd2m2 A3
#define ppwmm2 10
#define Kpm2 800

// motor 3
#define pd1m3 A4
#define pd2m3 A5
#define ppwmm3 9
#define Kpm3 1250

// motor 4
#define pd1m4 A6
#define pd2m4 A7
#define ppwmm4 8
#define Kpm4 1250

// motor 5
#define pd1m5 4
#define pd2m5 3
#define ppwmm5 2
#define Kpm5 1250

// motor 6
#define pd1m6 6
#define pd2m6 5
#define ppwmm6 7
#define Kpm6 2000

BDispenser m1(pd1m1, pd2m1, ppwmm1);
BDispenser m2(pd1m2, pd2m2, ppwmm2);
BDispenser m3(pd1m3, pd2m3, ppwmm3);
BDispenser m4(pd1m4, pd2m4, ppwmm4);
BDispenser m5(pd1m5, pd2m5, ppwmm5);

BDispenser dispensers[5] = {m1, m2, m3, m4, m5};

// balance parameters

float setg = 0;

#define CC_CLK 12
#define CC_DAT 13
#define calibrationfactor 965500 // 1 kg load cell

unsigned long sampletime = 100;

HX711 Balance(CC_DAT, CC_CLK);

// themocuple parameters

#define thermoDO A15
#define thermoCS A14
#define thermoCLK A13

MAX6675 tsensor(thermoCLK, thermoCS, thermoDO);

float temp = 0;
unsigned long stime = 3000;
unsigned long st = 0;
#define MIX_TEMP 90

// buzzer parameters

#define buzzpin A8

// rgb parameters

#define RPIN 14
#define GPIN 15
#define BPIN 16

// machine parameters

int _state = 0;

int dispenser_stage = 0;
int nstages = 5;
float settemp = 0;
boolean dispensing = false;
boolean mixing = false;
boolean waitsignal = false;
unsigned long readybeep = 1000;
boolean yestone = true;

float grams = 0;

void setupbalance() {

  Balance.set_scale();
  Balance.tare();
  Balance.read_average();
  Balance.set_scale(calibrationfactor);
  Balance.tare();

}

void setupbuzzer() {

  pinMode(buzzpin, OUTPUT);

}

void setuprgb() {

  pinMode(RPIN, OUTPUT);
  pinMode(GPIN, OUTPUT);
  pinMode(BPIN, OUTPUT);

}

void lights(int r, int g, int b) {

  digitalWrite(RPIN, r);
  digitalWrite(GPIN, g);
  digitalWrite(BPIN, b);

}

void setupdispensers() {

  dispensers[0].setkp(Kpm1);
  dispensers[0].assign("pump", "water", 1.0);
  dispensers[1].setkp(Kpm2);
  dispensers[1].assign("ppump", "glycerine", 1.26);
  dispensers[2].setkp(Kpm3);
  dispensers[2].assign("powderd", "agar-agar", 1.0);
  dispensers[3].setkp(Kpm4);
  dispensers[3].assign("powderd", "CA propanoate", 1.0);
  dispensers[4].setkp(Kpm5);
  dispensers[4].assign("powderd", "apple", 1.0);

}

void setup() {

  setupbalance();
  setupbuzzer();
  setuprgb();
  setupdispensers();

  Serial.begin(9600);

  buzz(200, 800);
  delay(10);
  buzz(200, 900);
  delay(10);
  buzz(200, 1000);

    setState(ST_IDLE);
}

void setState(int state) {

  switch (state) {
    case ST_IDLE:
      lights(0, 1, 0);
      break;
    case ST_DISPENSING:
      lights(0,0,1);
      break;
    case ST_MIXING:
      lights(1,0,1);
      break;
    case ST_WAIT_TEMP:
      lights(1,0,0);      
      break;
    case ST_END_PROCCESS:
      break;
  }
  _state = state;
}

void parseCommand() {

  int cmd = packet.unpacked.cmd;

  switch (cmd) {
    case CMD_SET_VALUES:
      // send back the packet for debugging
      for (int i = 0; i < numBytes; i++) {
        Serial.write(packet.bytes[i]);
      }
      for (int i = 0; i < dataValues; i++) {
        values[i] = packet.unpacked.data[i];
      }
      buzzBuzzer(500, 650);
      setState(ST_IDLE);
      blinkLed(200);
      buzzBuzzer(1000, 550);
      break;

    case CMD_START:
      //setState(ST_IDLE);
      setState(ST_DISPENSING);
/*
      for(int i=0; i<dataValues; i++){
        Serial.println(values[i]);
	delay(1000);
      }
*/
      blinkLed(200);
      buzz(200, 600);
      buzz(200, 800);
      buzz(200, 1000);
      break;

    case CMD_STOP:
      setState(ST_IDLE);
      buzz(200, 1000);
      buzz(200, 800);
      buzz(200, 600);
      break;

    case CMD_RGB_DEBUG:
      for (int i = 0; i < numBytes; i++) {
        Serial.write(packet.bytes[i]);
      }
      for (int i = 0; i < dataValues; i++) {
        values[i] = packet.unpacked.data[i];
      }
      lights(values[0],values[1],values[2]);
      buzzBuzzer(1000,220);
  }

  // clear data structure
  for (int i = 0; i < numBytes; i++) {
    packet.bytes[i] = 0;
  }

  Serial.flush();
}

/*
   stage 0: idle
   stage 1: dispensing
   stage 2: mixing
   stage 3: set temp
*/

void loop() {

  readPacket();
  parseCommand();

  if (_state == ST_DISPENSING) {
    while (dispenser_stage < nstages) {
      Serial.println(dispensers[dispenser_stage].bmaterial());
      //dispensing stage
      setg = float(values[dispenser_stage]);
      if (setg == 0) {
        dispenser_stage = dispenser_stage + 1;
      }
      else if (setg > 0) {
        dispensing = true;
        Balance.tare();
      }
      if (dispensing == true) {
        while(true){
          if (millis() - st >= sampletime) {
            float grams = Balance.get_units() * 1000;
            //Serial.println(grams);
            dispensers[dispenser_stage].dispense(setg, grams);
            st = millis();
            lights(1, 0, 0);
          }
          if (dispensers[dispenser_stage].isReady() == true) {
            dispensing = false;
            dispensers[dispenser_stage].turnoff();
            setg = 50000;
            dispenser_stage = dispenser_stage + 1; //pasa al siguiente dispensador
            buzzBuzzer(500, 440);
            lights(0, 0, 1);
            break;
          }
        }
      }
    }
    setState(ST_WAIT_TEMP);
    //Serial.println("mixing");
    settemp = MIX_TEMP;
  }

  while (_state == ST_WAIT_TEMP) {
    if (millis() - st >= stime) {
      lights(1, 0, 0);
      temp = tsensor.readCelsius();
      Serial.println(temp);
      st = millis();
      lights(1, 0, 0);
    }

    if (temp >= settemp) {
      buzzBuzzer(500, 600);
      settemp = 0;
      setState(ST_IDLE);
      //Serial.println("READY");
    }
  }
}

void blinkLed(int t) {
  digitalWrite(LED_BUILTIN, HIGH);
  delay(t / 2);
  digitalWrite(LED_BUILTIN, LOW);
  delay(t / 2);
  digitalWrite(LED_BUILTIN, HIGH);
  delay(t / 2);
  digitalWrite(LED_BUILTIN, LOW);
  delay(t / 2);
}

void buzzBuzzer(int t, int ton) {
  tone(buzzpin, ton, 500);
  delay(t / 3);
  tone(buzzpin, ton + 100, 500);
  delay(t / 3);
  tone(buzzpin, ton, 500);
  delay(t / 3);
  noTone(buzzpin);
}

void buzz(int t, int ton) {
  tone(buzzpin, ton, 500);
  delay(t);
  noTone(buzzpin);
}
