#include "BDispenser.h"
#include "HX711.h"
#include "max6675.h"

// dispenser parameters
// motor 1
#define pd1m1 A0
#define pd2m1 A1
#define ppwmm1 11
#define Kpm1 25

// motor 2
#define pd1m2 A2
#define pd2m2 A3
#define ppwmm2 10
#define Kpm2 800

// motor 3 agar
#define pd1m3 A4
#define pd2m3 A5
#define ppwmm3 9
#define Kpm3 1250

// motor 4
#define pd1m4 A6
#define pd2m4 A7
#define ppwmm4 8
#define Kpm4 1250

// motor 5
#define pd1m5 4
#define pd2m5 3
#define ppwmm5 2
#define Kpm5 1250

// motor 6
#define pd1m6 6
#define pd2m6 5
#define ppwmm6 7
#define Kpm6 2000


BDispenser m1(pd1m1, pd2m1, ppwmm1);
BDispenser m2(pd1m2, pd2m2, ppwmm2);
BDispenser m3(pd1m3, pd2m3, ppwmm3);
BDispenser m4(pd1m4, pd2m4, ppwmm4);
BDispenser m5(pd1m5, pd2m5, ppwmm5);

BDispenser dispensers[5] = {m1, m2, m3, m4, m5};

// balance parameters

float setg = 0;

#define CC_CLK 12
#define CC_DAT 13
#define calibrationfactor 965500 // 1 kg load cell

unsigned long sampletime = 100;

HX711 Balance(CC_DAT, CC_CLK);

// themocuple parameters

#define thermoDO A15
#define thermoCS A14
#define thermoCLK A13

MAX6675 tsensor(thermoCLK, thermoCS, thermoDO);

float temp = 0;
unsigned long stime = 2000;
unsigned long st = 0;

// buzzer parameters

#define buzzpin A8

// rgb parameters

#define RPIN 14
#define GPIN 15
#define BPIN 16

// machine parameters

int nstages = 5;
int stage = 0;
float settemp = 0;
boolean dispensing = false;
boolean mixing = false;
boolean waitsignal = false;
unsigned long readybeep = 1000;
boolean yestone = true;


void setupbalance() {

  Balance.set_scale();
  Balance.tare();
  Balance.read_average();
  Balance.set_scale(calibrationfactor);
  Balance.tare();

}

void setupbuzzer() {

  pinMode(buzzpin, OUTPUT);

}

void setuprgb() {

  pinMode(RPIN, OUTPUT);
  pinMode(GPIN, OUTPUT);
  pinMode(BPIN, OUTPUT);

}

void lights(int r, int g, int b) {

  digitalWrite(RPIN, r);
  digitalWrite(GPIN, g);
  digitalWrite(BPIN, b);

}

void setupdispensers() {

  dispensers[0].setkp(Kpm1);
  dispensers[0].assign("pump", "water", 1.0);
  dispensers[1].setkp(Kpm2);
  dispensers[1].assign("pump", "glycerine", 1.26); 
  dispensers[2].setkp(Kpm3);
  dispensers[2].assign("powder", "agar-agar", 1.0);
  dispensers[3].setkp(Kpm4);
  dispensers[3].assign("powder", "CA propanoate", 1.0);
  dispensers[4].setkp(Kpm5);
  dispensers[4].assign("powder", "residue", 1.0);

}

void setup() {

  setupbalance();
  setupbuzzer();
  setuprgb();
  setupdispensers();
  Serial.begin(9600);
  Serial.println("BIOMIXER V2 SETTING UP");
  delay(500);
  Serial.println("READY FOR MAKING");
  tone(buzzpin, 440, 500);
  delay(1000);
  noTone(buzzpin);
  //message to user

}

void loop() {

  lights(0,1,0);

  if (stage < nstages && dispensing == false) { //dispensing stage

    Serial.print("How much ");
    Serial.print(dispensers[stage].bmaterial());
    if (dispensers[stage].btype() == "powder") {
      Serial.println("[gr]?: ");
    }
    else {
      Serial.println("[ml]?: ");
    }

    while (true) {
      if (Serial.available() > 0) {
        setg = Serial.parseFloat();
        float aux = Serial.parseFloat();
        Serial.println(setg);
        //Serial.println(aux);

        //Serial.flush();
        if (setg <= 0.0) {
          stage = stage + 1;
          Balance.tare();
        }
        else if (setg > 0) {
          dispensing = true;
          Balance.tare();
        }
        break;
      }
    }


    if (dispensing == true) {
      while (true) {
        if (millis() - st >= sampletime) {
          float grams = Balance.get_units() * 1000;
          Serial.print(setg);
          Serial.print("     ");
          Serial.println(grams);
          dispensers[stage].dispense(setg, grams);
          st = millis();
          lights(1,0,0);
        }

        if (dispensers[stage].isReady() == true) {
          dispensing = false;
          dispensers[stage].turnoff();
          stage = stage + 1;
          setg = 50000;
          tone(buzzpin, 440, 500);
          delay(1000);
          noTone(buzzpin);
          lights(0,0,1);
          break;
        }
      }
    }
  }
  else {  // all ingredients dispensed
    if (mixing == false) {
      Serial.println("Target temperature for mixing[°C]?: ");
      //mixing stage
      while (true) {
        if (Serial.available() > 0) {
          settemp = Serial.parseFloat();
          float aux2 = Serial.parseFloat();
          if (settemp > 0) {
            mixing = true;
            lights(1,0,0);
            break;
          }
        }
      }
    }

    if (mixing == true) {

      if (millis() - st >= stime && waitsignal == false) {
        temp = tsensor.readCelsius();
        Serial.println(temp);
        st = millis();
      }

      if (temp >= settemp && waitsignal == false) {
        waitsignal = true;
        settemp = 0;
        Serial.println("REMOVE VASE AND SEND NUMBER '1'");

      }

      if (millis() - st >= readybeep && waitsignal == true) {
        yestone = !yestone;
        st = millis();
        //Serial.println("lawea");
        //Serial.println(yestone);
      }

      if (waitsignal == true) {
        if (yestone == true) {
          tone(buzzpin, 440, 500);
        }
        else {
          noTone(buzzpin);
        }
      }

      if (waitsignal == true) {
        if (Serial.available() > 0) {
          int endnum = Serial.parseInt();
          int aux3 = Serial.parseInt();
          if (endnum == 1) {
            mixing = false;
            waitsignal = false;
          }
        }
      }

      if (mixing == false) {
        stage = 0;
        dispensing = false;
        tone(buzzpin, 440, 500);
        delay(1000);
        noTone(buzzpin);
        lights(0,1,0);
        Serial.println("MIXING COMPLETE");
        Serial.println("-----------------");
        Serial.println("Ready for making");
      }
    }

  }
  Serial.flush();
}
